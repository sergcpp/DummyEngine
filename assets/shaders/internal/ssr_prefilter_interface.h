#ifndef SSR_PREFILTER_INTERFACE_H
#define SSR_PREFILTER_INTERFACE_H

#include "_interface_common.h"

INTERFACE_START(SSRPrefilter)

struct Params {
    UVEC2_TYPE img_size;
    VEC2_TYPE thresholds;
};

DEF_CONST_INT(LOCAL_GROUP_SIZE_X, 8)
DEF_CONST_INT(LOCAL_GROUP_SIZE_Y, 8)

DEF_CONST_INT(DEPTH_TEX_SLOT, 2)
DEF_CONST_INT(NORM_TEX_SLOT, 3)
DEF_CONST_INT(AVG_REFL_TEX_SLOT, 4)
DEF_CONST_INT(REFL_TEX_SLOT, 5)
DEF_CONST_INT(VARIANCE_TEX_SLOT, 6)
DEF_CONST_INT(SAMPLE_COUNT_TEX_SLOT, 7)
DEF_CONST_INT(TILE_LIST_BUF_SLOT, 8)

DEF_CONST_INT(OUT_REFL_IMG_SLOT, 0)
DEF_CONST_INT(OUT_VARIANCE_IMG_SLOT, 1)

INTERFACE_END

#endif // SSR_PREFILTER_INTERFACE_H