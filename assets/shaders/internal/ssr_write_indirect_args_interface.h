#ifndef SSR_WRITE_INDIRECT_ARGS_INTERFACE_H
#define SSR_WRITE_INDIRECT_ARGS_INTERFACE_H

#include "_interface_common.h"

INTERFACE_START(SSRWriteIndirectArgs)

DEF_CONST_INT(RAY_COUNTER_SLOT, 0)
DEF_CONST_INT(INDIR_ARGS_SLOT, 1)

INTERFACE_END

#endif // SSR_WRITE_INDIRECT_ARGS_INTERFACE_H