#ifndef RT_DEBUG_INTERFACE_H
#define RT_DEBUG_INTERFACE_H

#include "_interface_common.h"

INTERFACE_START(RTDebug)

struct Params {
    UVEC2_TYPE img_size;
    float pixel_spread_angle;
    UINT_TYPE root_node;
};

struct RayPayload {
    VEC3_TYPE col;
    float cone_width;
};

DEF_CONST_INT(MAX_STACK_SIZE, 48)

DEF_CONST_INT(LOCAL_GROUP_SIZE_X, 8)
DEF_CONST_INT(LOCAL_GROUP_SIZE_Y, 8)

DEF_CONST_INT(TLAS_SLOT, 2)
DEF_CONST_INT(ENV_TEX_SLOT, 3)
DEF_CONST_INT(GEO_DATA_BUF_SLOT, 4)
DEF_CONST_INT(MATERIAL_BUF_SLOT, 5)
DEF_CONST_INT(VTX_BUF1_SLOT, 6)
DEF_CONST_INT(VTX_BUF2_SLOT, 7)
DEF_CONST_INT(NDX_BUF_SLOT, 8)
DEF_CONST_INT(BLAS_BUF_SLOT, 9)
DEF_CONST_INT(TLAS_BUF_SLOT, 10)
DEF_CONST_INT(PRIM_NDX_BUF_SLOT, 11)
DEF_CONST_INT(MESHES_BUF_SLOT, 12)
DEF_CONST_INT(MESH_INSTANCES_BUF_SLOT, 13)
DEF_CONST_INT(LMAP_TEX_SLOTS, 14)
DEF_CONST_INT(OUT_IMG_SLOT, 1)

INTERFACE_END

#endif // RT_DEBUG_INTERFACE_H