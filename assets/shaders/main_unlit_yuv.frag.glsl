#version 310 es
#extension GL_EXT_texture_buffer : enable
#extension GL_OES_texture_buffer : enable
#extension GL_EXT_texture_cube_map_array : enable
//#extension GL_EXT_control_flow_attributes : enable

$ModifyWarning

#if defined(GL_ES) || defined(VULKAN)
    precision highp int;
    precision mediump float;
    precision mediump sampler2DShadow;
#endif

#include "internal/_fs_common.glsl"
#include "internal/_texturing.glsl"

#define LIGHT_ATTEN_CUTOFF 0.004

#if !defined(BINDLESS_TEXTURES)
layout(binding = REN_MAT_TEX0_SLOT) uniform sampler2D g_mat0_tex;
layout(binding = REN_MAT_TEX1_SLOT) uniform sampler2D g_mat1_tex;
#endif // BINDLESS_TEXTURES

#if defined(VULKAN) || defined(GL_SPIRV)
layout (binding = REN_UB_SHARED_DATA_LOC, std140)
#else
layout (std140)
#endif
uniform SharedDataBlock {
    SharedData g_shrd_data;
};

LAYOUT(location = 4) in vec2 g_vtx_uvs0;
#if defined(BINDLESS_TEXTURES)
    LAYOUT(location = 8) in flat TEX_HANDLE g_mat0_tex;
    LAYOUT(location = 9) in flat TEX_HANDLE g_mat1_tex;
#endif // BINDLESS_TEXTURES

layout(location = REN_OUT_COLOR_INDEX) out vec4 g_out_color;
layout(location = REN_OUT_NORM_INDEX) out vec4 g_out_normal;

void main(void) {
    vec3 col_yuv;
    col_yuv.x = texture(SAMPLER2D(g_mat0_tex), g_vtx_uvs0).r;
    col_yuv.yz = texture(SAMPLER2D(g_mat1_tex), g_vtx_uvs0).rg;
    col_yuv += vec3(-0.0627451017, -0.501960814, -0.501960814);

    vec3 col_rgb;
    col_rgb.r = dot(col_yuv, vec3(1.164,  0.000,  1.596));
    col_rgb.g = dot(col_yuv, vec3(1.164, -0.391, -0.813));
    col_rgb.b = dot(col_yuv, vec3(1.164,  2.018,  0.000));

    g_out_color = vec4(SRGBToLinear(col_rgb), 1.0);
    g_out_normal = vec4(0.0);
}
