gl_program: main_probelit_blend main_probelit.vert.glsl main_probelit_blend.frag.glsl
gl_program: main_probelit_blend main_probelit.vert.glsl main_probelit_blend.frag.glsl
sw_program: main
flag: two_sided
flag: alpha_blend
flag: alpha_test
texture: chars/embed_hair_male_diff.png
texture: default_normalmap.dds
texture: default_specular.png
texture: chars/embed_hair_male_mask.png
