#pragma once

#include <Ren/Texture.h>
#include <Ren/VertexInput.h>

#include "../Graph/SubPass.h"
#include "../Renderer_DrawList.h"

class PrimDraw;
struct ViewState;

struct RpRTShadowsData {
    RpResRef geo_data;
    RpResRef materials;
    RpResRef vtx_buf1;
    RpResRef ndx_buf;
    RpResRef shared_data;
    RpResRef noise_tex;
    RpResRef depth_tex;
    RpResRef normal_tex;
    RpResRef tlas_buf;
    RpResRef tile_list_buf;
    RpResRef indir_args;

    Ren::IAccStructure *tlas = nullptr;

    struct {
        uint32_t root_node;
        RpResRef blas_buf;
        RpResRef prim_ndx_buf;
        RpResRef meshes_buf;
        RpResRef mesh_instances_buf;
        RpResRef textures_buf;
    } swrt;

    RpResRef out_shadow_tex;
};

class RpRTShadows : public RpExecutor {
    bool initialized = false;

    // lazily initialized data
    Ren::Pipeline pi_rt_shadows_;
    Ren::Pipeline pi_rt_shadows_inline_;
    Ren::Pipeline pi_rt_shadows_swrt_;

    // temp data (valid only between Setup and Execute calls)
    const ViewState *view_state_ = nullptr;
    const BindlessTextureData *bindless_tex_ = nullptr;

    const RpRTShadowsData *pass_data_;

    void LazyInit(Ren::Context &ctx, ShaderLoader &sh);

    void Execute_HWRT_Pipeline(RpBuilder &builder);
    void Execute_HWRT_Inline(RpBuilder &builder);
    void Execute_SWRT(RpBuilder &builder);

  public:
    void Setup(RpBuilder &builder, const ViewState *view_state, const BindlessTextureData *bindless_tex,
               const RpRTShadowsData *pass_data) {
        view_state_ = view_state;
        bindless_tex_ = bindless_tex;
        pass_data_ = pass_data;
    }

    void Execute(RpBuilder &builder) override;
};