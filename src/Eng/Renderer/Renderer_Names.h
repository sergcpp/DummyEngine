#pragma once

const char INSTANCES_BUF[] = "Instances Buffer";
const char INSTANCE_INDICES_BUF[] = "Instance Indices Buffer";
const char CELLS_BUF[] = "Cells Buffer";
const char LIGHTS_BUF[] = "Lights Buffer";
const char DECALS_BUF[] = "Decals Buffer";
const char ITEMS_BUF[] = "Items Buffer";
const char SHARED_DATA_BUF[] = "Shared Data Buffer";
const char ATOMIC_CNT_BUF[] = "Atomic Counter Buffer";

    const char SKIN_TRANSFORMS_BUF[] = "Skin Transforms Buffer";
const char SHAPE_KEYS_BUF[] = "Shape Keys Buffer";

//

const char SHADOWMAP_TEX[] = "Shadow Map";

const char MAIN_COLOR_TEX[] = "Main Color";
const char MAIN_NORMAL_TEX[] = "Main Normals";
const char MAIN_SPEC_TEX[] = "Main Specular";
const char MAIN_DEPTH_TEX[] = "Main Depth";
const char MAIN_VELOCITY_TEX[] = "Main Velocity";
const char MAIN_ALBEDO_TEX[] = "Main Albedo";

const char MAIN_COMBINED_TEX[] = "Main Combined";

const char RESOLVED_COLOR_TEX[] = "Resolved Color";
const char DEPTH_DOWN_2X_TEX[] = "Depth Down 2x";
const char DEPTH_DOWN_4X_TEX[] = "Depth Down 4x";
const char DEPTH_HIERARCHY_TEX[] = "Depth Hierarchy";

const char SSAO_RAW[] = "SSAO RAW";
const char SSAO_RES[] = "SSAO RES";

const char BLUR_RES_TEX[] = "BLUR RES";

const char DOF_COLOR_TEX[] = "DOF Color";

const char REDUCED_TEX[] = "REDUCED";