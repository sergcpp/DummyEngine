#include "Transform.h"

#include <Sys/Json.h>

void Transform::UpdateBBox() {
    bbox_min_ws = bbox_max_ws = Ren::Vec3f(world_from_object[3]);

    for (int j = 0; j < 3; j++) {
        for (int i = 0; i < 3; i++) {
            const float a = world_from_object[i][j] * bbox_min[i];
            const float b = world_from_object[i][j] * bbox_max[i];

            if (a < b) {
                bbox_min_ws[j] += a;
                bbox_max_ws[j] += b;
            } else {
                bbox_min_ws[j] += b;
                bbox_max_ws[j] += a;
            }
        }
    }
}

void Transform::UpdateInvMatrix() { object_from_world = Inverse(world_from_object); }

void Transform::Read(const JsObjectP &js_in, Transform &tr) {
    tr.world_from_object = Ren::Mat4f{1.0f};

    if (js_in.Has("pos")) {
        const JsArrayP &js_pos = js_in.at("pos").as_arr();

        const auto pos = Ren::Vec3f{float(js_pos.at(0).as_num().val), float(js_pos.at(1).as_num().val),
                                    float(js_pos.at(2).as_num().val)};

        tr.world_from_object = Translate(tr.world_from_object, pos);
    }

    if (js_in.Has("rot")) {
        const JsArrayP &js_rot = js_in.at("rot").as_arr();

        // angles in degrees
        tr.euler_angles_rad = Ren::Vec3f{float(js_rot.at(0).as_num().val), float(js_rot.at(1).as_num().val),
                                         float(js_rot.at(2).as_num().val)};

        // convert to radians
        tr.euler_angles_rad *= Ren::Pi<float>() / 180.0f;

        const Ren::Mat4f rot_z = Rotate(Ren::Mat4f{1.0f}, tr.euler_angles_rad[2], Ren::Vec3f{0.0f, 0.0f, 1.0f}),
                         rot_x = Rotate(Ren::Mat4f{1.0f}, tr.euler_angles_rad[0], Ren::Vec3f{1.0f, 0.0f, 0.0f}),
                         rot_y = Rotate(Ren::Mat4f{1.0f}, tr.euler_angles_rad[1], Ren::Vec3f{0.0f, 1.0f, 0.0f});

        const Ren::Mat4f rot_all = rot_y * rot_x * rot_z;
        tr.world_from_object = tr.world_from_object * rot_all;
    }

    if (js_in.Has("scale")) {
        const JsArrayP &js_scale = js_in.at("scale").as_arr();

        tr.scale[0] = float(js_scale[0].as_num().val);
        tr.scale[1] = float(js_scale[1].as_num().val);
        tr.scale[2] = float(js_scale[2].as_num().val);

        tr.world_from_object = Scale(tr.world_from_object, tr.scale);
    } else {
        tr.scale = Ren::Vec3f{1.0f, 1.0f, 1.0f};
    }

    tr.UpdateInvMatrix();
}

void Transform::Write(const Transform &tr, JsObjectP &js_out) {
    const auto &alloc = js_out.elements.get_allocator();

    { // write position
        JsArrayP js_pos(alloc);

        js_pos.Push(JsNumber{tr.world_from_object[3][0]});
        js_pos.Push(JsNumber{tr.world_from_object[3][1]});
        js_pos.Push(JsNumber{tr.world_from_object[3][2]});

        js_out.Push("pos", std::move(js_pos));
    }

    { // write rotation
        JsArrayP js_rot(alloc);

        const Ren::Vec3f euler_angles_deg = tr.euler_angles_rad * 180.0f / Ren::Pi<float>();

        js_rot.Push(JsNumber{euler_angles_deg[0]});
        js_rot.Push(JsNumber{euler_angles_deg[1]});
        js_rot.Push(JsNumber{euler_angles_deg[2]});

        js_out.Push("rot", std::move(js_rot));
    }

    if (tr.scale[0] != 1.0f || tr.scale[1] != 1.0f || tr.scale[2] != 1.0f) {
        JsArrayP js_scale(alloc);

        js_scale.Push(JsNumber{tr.scale[0]});
        js_scale.Push(JsNumber{tr.scale[1]});
        js_scale.Push(JsNumber{tr.scale[2]});

        js_out.Push("scale", std::move(js_scale));
    }
}

//
// Euler angles from matrix (maybe will be needed later)
//
/*
    // https://www.gregslabaugh.net/publications/euler.pdf

    float theta_x, theta_z;
    float theta_y = std::asin(tr.mat[2][0]);
    if (theta_y < Ren::Pi<float>() / 2.0f ) {
        if (theta_y > -Ren::Pi<float>() / 2.0f) {
            theta_x = std::atan2(-tr.mat[2][1], tr.mat[2][2]);
            theta_z = std::atan2(-tr.mat[1][0], tr.mat[0][0]);
        } else {
            // not a unique solution
            theta_x = -std::atan2(tr.mat[0][1], tr.mat[1][1]);
            theta_z = 0.0f;
        }
    } else {
        // not a unique solution
        theta_x = std::atan2(tr.mat[0][1], tr.mat[1][1]);
        theta_z = 0.0f;
    }

    // convert to degrees
    theta_x *= 180.0f / Ren::Pi<float>();
    theta_y *= 180.0f / Ren::Pi<float>();
    theta_z *= 180.0f / Ren::Pi<float>();

*/