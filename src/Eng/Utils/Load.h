#pragma once

#include <string>
#include <vector>

std::vector<uint8_t> LoadHDR(const char *name, int &w, int &h);