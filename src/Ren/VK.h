#pragma once

#define VK_NO_PROTOTYPES
#define VK_ENABLE_BETA_EXTENSIONS
#include "vulkan/vulkan.h"
#undef far
#undef near
#undef max
#undef min
#undef None
#undef Success

#ifdef __VK_API_DEF__
#define EXTERN_FUNC
#else
#define EXTERN_FUNC extern
#endif

#define vkCreateInstance ren_vkCreateInstance
#define vkDestroyInstance ren_vkDestroyInstance
#define vkEnumerateInstanceLayerProperties ren_vkEnumerateInstanceLayerProperties
#define vkEnumerateInstanceExtensionProperties ren_vkEnumerateInstanceExtensionProperties
#define vkGetInstanceProcAddr ren_vkGetInstanceProcAddr

#define vkEnumeratePhysicalDevices ren_vkEnumeratePhysicalDevices
#define vkGetPhysicalDeviceProperties ren_vkGetPhysicalDeviceProperties
#define vkGetPhysicalDeviceProperties2KHR ren_vkGetPhysicalDeviceProperties2KHR
#define vkGetPhysicalDeviceFeatures ren_vkGetPhysicalDeviceFeatures
#define vkGetPhysicalDeviceQueueFamilyProperties ren_vkGetPhysicalDeviceQueueFamilyProperties

#define vkCreateDevice ren_vkCreateDevice
#define vkDestroyDevice ren_vkDestroyDevice

#define vkEnumerateDeviceExtensionProperties ren_vkEnumerateDeviceExtensionProperties

#define vkCreateDebugReportCallbackEXT ren_vkCreateDebugReportCallbackEXT
#define vkDestroyDebugReportCallbackEXT ren_vkDestroyDebugReportCallbackEXT
#define vkDebugReportMessageEXT ren_vkDebugReportMessageEXT

#if defined(VK_USE_PLATFORM_WIN32_KHR)
#define vkCreateWin32SurfaceKHR ren_vkCreateWin32SurfaceKHR
#elif defined(VK_USE_PLATFORM_XLIB_KHR)
#define vkCreateXlibSurfaceKHR ren_vkCreateXlibSurfaceKHR
#elif defined(VK_USE_PLATFORM_IOS_MVK)
#define vkCreateIOSSurfaceMVK ren_vkCreateIOSSurfaceMVK
#endif
#define vkDestroySurfaceKHR ren_vkDestroySurfaceKHR
#define vkGetPhysicalDeviceSurfaceSupportKHR ren_vkGetPhysicalDeviceSurfaceSupportKHR
#define vkGetPhysicalDeviceSurfaceCapabilitiesKHR ren_vkGetPhysicalDeviceSurfaceCapabilitiesKHR
#define vkGetPhysicalDeviceSurfaceFormatsKHR ren_vkGetPhysicalDeviceSurfaceFormatsKHR

#define vkGetPhysicalDeviceSurfacePresentModesKHR ren_vkGetPhysicalDeviceSurfacePresentModesKHR
#define vkCreateSwapchainKHR ren_vkCreateSwapchainKHR
#define vkDestroySwapchainKHR ren_vkDestroySwapchainKHR
#define vkGetDeviceQueue ren_vkGetDeviceQueue
#define vkCreateCommandPool ren_vkCreateCommandPool
#define vkDestroyCommandPool ren_vkDestroyCommandPool
#define vkAllocateCommandBuffers ren_vkAllocateCommandBuffers
#define vkFreeCommandBuffers ren_vkFreeCommandBuffers
#define vkDeviceWaitIdle ren_vkDeviceWaitIdle

#define vkGetSwapchainImagesKHR ren_vkGetSwapchainImagesKHR

#define vkCreateFence ren_vkCreateFence
#define vkWaitForFences ren_vkWaitForFences
#define vkResetFences ren_vkResetFences
#define vkDestroyFence ren_vkDestroyFence
#define vkGetFenceStatus ren_vkGetFenceStatus

#define vkBeginCommandBuffer ren_vkBeginCommandBuffer
#define vkEndCommandBuffer ren_vkEndCommandBuffer
#define vkCmdPipelineBarrier ren_vkCmdPipelineBarrier

#define vkQueueSubmit ren_vkQueueSubmit
#define vkQueueWaitIdle ren_vkQueueWaitIdle
#define vkResetCommandBuffer ren_vkResetCommandBuffer
#define vkCreateImageView ren_vkCreateImageView
#define vkDestroyImageView ren_vkDestroyImageView

#define vkAcquireNextImageKHR ren_vkAcquireNextImageKHR
#define vkQueuePresentKHR ren_vkQueuePresentKHR

#define vkGetPhysicalDeviceMemoryProperties ren_vkGetPhysicalDeviceMemoryProperties
#define vkGetPhysicalDeviceFormatProperties ren_vkGetPhysicalDeviceFormatProperties
#define vkGetPhysicalDeviceImageFormatProperties ren_vkGetPhysicalDeviceImageFormatProperties

#define vkCreateImage ren_vkCreateImage
#define vkDestroyImage ren_vkDestroyImage

#define vkGetImageMemoryRequirements ren_vkGetImageMemoryRequirements
#define vkAllocateMemory ren_vkAllocateMemory
#define vkFreeMemory ren_vkFreeMemory
#define vkBindImageMemory ren_vkBindImageMemory

#define vkCreateRenderPass ren_vkCreateRenderPass
#define vkDestroyRenderPass ren_vkDestroyRenderPass

#define vkCreateFramebuffer ren_vkCreateFramebuffer
#define vkDestroyFramebuffer ren_vkDestroyFramebuffer

#define vkCreateBuffer ren_vkCreateBuffer
#define vkGetBufferDeviceAddressKHR ren_vkGetBufferDeviceAddressKHR
#define vkBindBufferMemory ren_vkBindBufferMemory
#define vkDestroyBuffer ren_vkDestroyBuffer

#define vkCreateBufferView ren_vkCreateBufferView
#define vkDestroyBufferView ren_vkDestroyBufferView

#define vkMapMemory ren_vkMapMemory
#define vkUnmapMemory ren_vkUnmapMemory
#define vkFlushMappedMemoryRanges ren_vkFlushMappedMemoryRanges
#define vkInvalidateMappedMemoryRanges ren_vkInvalidateMappedMemoryRanges

#define vkCreateShaderModule ren_vkCreateShaderModule
#define vkDestroyShaderModule ren_vkDestroyShaderModule

#define vkCreateDescriptorSetLayout ren_vkCreateDescriptorSetLayout
#define vkDestroyDescriptorSetLayout ren_vkDestroyDescriptorSetLayout

#define vkCreatePipelineLayout ren_vkCreatePipelineLayout
#define vkDestroyPipelineLayout ren_vkDestroyPipelineLayout

#define vkCreateGraphicsPipelines ren_vkCreateGraphicsPipelines
#define vkCreateComputePipelines ren_vkCreateComputePipelines
#define vkDestroyPipeline ren_vkDestroyPipeline

#define vkCreateSemaphore ren_vkCreateSemaphore
#define vkDestroySemaphore ren_vkDestroySemaphore

#define vkCreateSampler ren_vkCreateSampler
#define vkDestroySampler ren_vkDestroySampler

#define vkCreateDescriptorPool ren_vkCreateDescriptorPool
#define vkDestroyDescriptorPool ren_vkDestroyDescriptorPool
#define vkResetDescriptorPool ren_vkResetDescriptorPool

#define vkAllocateDescriptorSets ren_vkAllocateDescriptorSets
#define vkFreeDescriptorSets ren_vkFreeDescriptorSets
#define vkUpdateDescriptorSets ren_vkUpdateDescriptorSets

#define vkCreateQueryPool ren_vkCreateQueryPool
#define vkDestroyQueryPool ren_vkDestroyQueryPool
#define vkGetQueryPoolResults ren_vkGetQueryPoolResults

#define vkCreateAccelerationStructureKHR ren_vkCreateAccelerationStructureKHR
#define vkDestroyAccelerationStructureKHR ren_vkDestroyAccelerationStructureKHR

#define vkCmdBeginRenderPass ren_vkCmdBeginRenderPass
#define vkCmdBindPipeline ren_vkCmdBindPipeline
#define vkCmdSetViewport ren_vkCmdSetViewport
#define vkCmdSetScissor ren_vkCmdSetScissor
#define vkCmdBindDescriptorSets ren_vkCmdBindDescriptorSets
#define vkCmdBindVertexBuffers ren_vkCmdBindVertexBuffers
#define vkCmdBindIndexBuffer ren_vkCmdBindIndexBuffer
#define vkCmdDraw ren_vkCmdDraw
#define vkCmdDrawIndexed ren_vkCmdDrawIndexed
#define vkCmdEndRenderPass ren_vkCmdEndRenderPass
#define vkCmdCopyBufferToImage ren_vkCmdCopyBufferToImage
#define vkCmdCopyImageToBuffer ren_vkCmdCopyImageToBuffer
#define vkCmdCopyBuffer ren_vkCmdCopyBuffer
#define vkCmdFillBuffer ren_vkCmdFillBuffer
#define vkCmdUpdateBuffer ren_vkCmdUpdateBuffer
#define vkCmdPushConstants ren_vkCmdPushConstants
#define vkCmdBlitImage ren_vkCmdBlitImage
#define vkCmdClearColorImage ren_vkCmdClearColorImage
#define vkCmdClearAttachments ren_vkCmdClearAttachments
#define vkCmdCopyImage ren_vkCmdCopyImage
#define vkCmdDispatch ren_vkCmdDispatch
#define vkCmdDispatchIndirect ren_vkCmdDispatchIndirect
#define vkCmdResetQueryPool ren_vkCmdResetQueryPool

#define vkCmdBeginRenderingKHR ren_vkCmdBeginRenderingKHR
#define vkCmdEndRenderingKHR ren_vkCmdEndRenderingKHR

#define vkCmdBuildAccelerationStructuresKHR ren_vkCmdBuildAccelerationStructuresKHR
#define vkCmdWriteAccelerationStructuresPropertiesKHR ren_vkCmdWriteAccelerationStructuresPropertiesKHR
#define vkCmdCopyAccelerationStructureKHR ren_vkCmdCopyAccelerationStructureKHR
#define vkCmdTraceRaysKHR ren_vkCmdTraceRaysKHR
#define vkCmdTraceRaysIndirectKHR ren_vkCmdTraceRaysIndirectKHR

#define vkCmdBeginDebugUtilsLabelEXT ren_vkCmdBeginDebugUtilsLabelEXT
#define vkCmdEndDebugUtilsLabelEXT ren_vkCmdEndDebugUtilsLabelEXT
#define vkSetDebugUtilsObjectNameEXT ren_vkSetDebugUtilsObjectNameEXT

#define vkCmdSetDepthBias ren_vkCmdSetDepthBias

#define vkGetBufferMemoryRequirements ren_vkGetBufferMemoryRequirements
#define vkGetAccelerationStructureBuildSizesKHR ren_vkGetAccelerationStructureBuildSizesKHR
#define vkGetAccelerationStructureDeviceAddressKHR ren_vkGetAccelerationStructureDeviceAddressKHR
#define vkGetRayTracingShaderGroupHandlesKHR ren_vkGetRayTracingShaderGroupHandlesKHR

#define vkCreateRayTracingPipelinesKHR ren_vkCreateRayTracingPipelinesKHR

#define vkCreateQueryPool ren_vkCreateQueryPool
#define vkCmdWriteTimestamp ren_vkCmdWriteTimestamp

extern "C" {
EXTERN_FUNC PFN_vkCreateInstance ren_vkCreateInstance;
EXTERN_FUNC PFN_vkDestroyInstance ren_vkDestroyInstance;
EXTERN_FUNC PFN_vkEnumerateInstanceLayerProperties ren_vkEnumerateInstanceLayerProperties;
EXTERN_FUNC PFN_vkEnumerateInstanceExtensionProperties ren_vkEnumerateInstanceExtensionProperties;
EXTERN_FUNC PFN_vkGetInstanceProcAddr ren_vkGetInstanceProcAddr;

EXTERN_FUNC PFN_vkEnumeratePhysicalDevices ren_vkEnumeratePhysicalDevices;
EXTERN_FUNC PFN_vkGetPhysicalDeviceProperties ren_vkGetPhysicalDeviceProperties;
EXTERN_FUNC PFN_vkGetPhysicalDeviceProperties2KHR ren_vkGetPhysicalDeviceProperties2KHR;
EXTERN_FUNC PFN_vkGetPhysicalDeviceFeatures ren_vkGetPhysicalDeviceFeatures;
EXTERN_FUNC PFN_vkGetPhysicalDeviceQueueFamilyProperties ren_vkGetPhysicalDeviceQueueFamilyProperties;

EXTERN_FUNC PFN_vkCreateDevice ren_vkCreateDevice;
EXTERN_FUNC PFN_vkDestroyDevice ren_vkDestroyDevice;

EXTERN_FUNC PFN_vkEnumerateDeviceExtensionProperties ren_vkEnumerateDeviceExtensionProperties;

EXTERN_FUNC PFN_vkCreateDebugReportCallbackEXT ren_vkCreateDebugReportCallbackEXT;
EXTERN_FUNC PFN_vkDestroyDebugReportCallbackEXT ren_vkDestroyDebugReportCallbackEXT;
EXTERN_FUNC PFN_vkDebugReportMessageEXT ren_vkDebugReportMessageEXT;

#if defined(VK_USE_PLATFORM_WIN32_KHR)
EXTERN_FUNC PFN_vkCreateWin32SurfaceKHR ren_vkCreateWin32SurfaceKHR;
#elif defined(VK_USE_PLATFORM_XLIB_KHR)
EXTERN_FUNC PFN_vkCreateXlibSurfaceKHR ren_vkCreateXlibSurfaceKHR;
#elif defined(VK_USE_PLATFORM_IOS_MVK)
EXTERN_FUNC PFN_vkCreateIOSSurfaceMVK ren_vkCreateIOSSurfaceMVK;
#elif defined(VK_USE_PLATFORM_MACOS_MVK)
EXTERN_FUNC PFN_vkCreateMacOSSurfaceMVK vkCreateMacOSSurfaceMVK;
#endif
EXTERN_FUNC PFN_vkDestroySurfaceKHR ren_vkDestroySurfaceKHR;
EXTERN_FUNC PFN_vkGetPhysicalDeviceSurfaceSupportKHR ren_vkGetPhysicalDeviceSurfaceSupportKHR;
EXTERN_FUNC PFN_vkGetPhysicalDeviceSurfaceCapabilitiesKHR ren_vkGetPhysicalDeviceSurfaceCapabilitiesKHR;
EXTERN_FUNC PFN_vkGetPhysicalDeviceSurfaceFormatsKHR ren_vkGetPhysicalDeviceSurfaceFormatsKHR;

EXTERN_FUNC PFN_vkGetPhysicalDeviceSurfacePresentModesKHR ren_vkGetPhysicalDeviceSurfacePresentModesKHR;
EXTERN_FUNC PFN_vkCreateSwapchainKHR ren_vkCreateSwapchainKHR;
EXTERN_FUNC PFN_vkDestroySwapchainKHR ren_vkDestroySwapchainKHR;
EXTERN_FUNC PFN_vkGetDeviceQueue ren_vkGetDeviceQueue;
EXTERN_FUNC PFN_vkCreateCommandPool ren_vkCreateCommandPool;
EXTERN_FUNC PFN_vkDestroyCommandPool ren_vkDestroyCommandPool;
EXTERN_FUNC PFN_vkAllocateCommandBuffers ren_vkAllocateCommandBuffers;
EXTERN_FUNC PFN_vkFreeCommandBuffers ren_vkFreeCommandBuffers;
EXTERN_FUNC PFN_vkDeviceWaitIdle ren_vkDeviceWaitIdle;

EXTERN_FUNC PFN_vkGetSwapchainImagesKHR ren_vkGetSwapchainImagesKHR;

EXTERN_FUNC PFN_vkCreateFence ren_vkCreateFence;
EXTERN_FUNC PFN_vkWaitForFences ren_vkWaitForFences;
EXTERN_FUNC PFN_vkResetFences ren_vkResetFences;
EXTERN_FUNC PFN_vkDestroyFence ren_vkDestroyFence;
EXTERN_FUNC PFN_vkGetFenceStatus ren_vkGetFenceStatus;

EXTERN_FUNC PFN_vkBeginCommandBuffer ren_vkBeginCommandBuffer;
EXTERN_FUNC PFN_vkEndCommandBuffer ren_vkEndCommandBuffer;
EXTERN_FUNC PFN_vkCmdPipelineBarrier ren_vkCmdPipelineBarrier;

EXTERN_FUNC PFN_vkQueueSubmit ren_vkQueueSubmit;
EXTERN_FUNC PFN_vkQueueWaitIdle ren_vkQueueWaitIdle;
EXTERN_FUNC PFN_vkResetCommandBuffer ren_vkResetCommandBuffer;
EXTERN_FUNC PFN_vkCreateImageView ren_vkCreateImageView;
EXTERN_FUNC PFN_vkDestroyImageView ren_vkDestroyImageView;

EXTERN_FUNC PFN_vkAcquireNextImageKHR ren_vkAcquireNextImageKHR;
EXTERN_FUNC PFN_vkQueuePresentKHR ren_vkQueuePresentKHR;

EXTERN_FUNC PFN_vkGetPhysicalDeviceMemoryProperties ren_vkGetPhysicalDeviceMemoryProperties;
EXTERN_FUNC PFN_vkGetPhysicalDeviceFormatProperties ren_vkGetPhysicalDeviceFormatProperties;
EXTERN_FUNC PFN_vkGetPhysicalDeviceImageFormatProperties ren_vkGetPhysicalDeviceImageFormatProperties;

EXTERN_FUNC PFN_vkCreateImage ren_vkCreateImage;
EXTERN_FUNC PFN_vkDestroyImage ren_vkDestroyImage;

EXTERN_FUNC PFN_vkGetImageMemoryRequirements ren_vkGetImageMemoryRequirements;
EXTERN_FUNC PFN_vkAllocateMemory ren_vkAllocateMemory;
EXTERN_FUNC PFN_vkFreeMemory ren_vkFreeMemory;
EXTERN_FUNC PFN_vkBindImageMemory ren_vkBindImageMemory;

EXTERN_FUNC PFN_vkCreateRenderPass ren_vkCreateRenderPass;
EXTERN_FUNC PFN_vkDestroyRenderPass ren_vkDestroyRenderPass;

EXTERN_FUNC PFN_vkCreateFramebuffer ren_vkCreateFramebuffer;
EXTERN_FUNC PFN_vkDestroyFramebuffer ren_vkDestroyFramebuffer;

EXTERN_FUNC PFN_vkCreateBuffer ren_vkCreateBuffer;
EXTERN_FUNC PFN_vkGetBufferDeviceAddressKHR ren_vkGetBufferDeviceAddressKHR;
EXTERN_FUNC PFN_vkBindBufferMemory ren_vkBindBufferMemory;
EXTERN_FUNC PFN_vkDestroyBuffer ren_vkDestroyBuffer;

EXTERN_FUNC PFN_vkCreateBufferView ren_vkCreateBufferView;
EXTERN_FUNC PFN_vkDestroyBufferView ren_vkDestroyBufferView;

EXTERN_FUNC PFN_vkMapMemory ren_vkMapMemory;
EXTERN_FUNC PFN_vkUnmapMemory ren_vkUnmapMemory;
EXTERN_FUNC PFN_vkFlushMappedMemoryRanges ren_vkFlushMappedMemoryRanges;
EXTERN_FUNC PFN_vkInvalidateMappedMemoryRanges ren_vkInvalidateMappedMemoryRanges;

EXTERN_FUNC PFN_vkCreateShaderModule ren_vkCreateShaderModule;
EXTERN_FUNC PFN_vkDestroyShaderModule ren_vkDestroyShaderModule;

EXTERN_FUNC PFN_vkCreateDescriptorSetLayout ren_vkCreateDescriptorSetLayout;
EXTERN_FUNC PFN_vkDestroyDescriptorSetLayout ren_vkDestroyDescriptorSetLayout;

EXTERN_FUNC PFN_vkCreatePipelineLayout ren_vkCreatePipelineLayout;
EXTERN_FUNC PFN_vkDestroyPipelineLayout ren_vkDestroyPipelineLayout;

EXTERN_FUNC PFN_vkCreateGraphicsPipelines ren_vkCreateGraphicsPipelines;
EXTERN_FUNC PFN_vkCreateComputePipelines ren_vkCreateComputePipelines;
EXTERN_FUNC PFN_vkDestroyPipeline ren_vkDestroyPipeline;

EXTERN_FUNC PFN_vkCreateSemaphore ren_vkCreateSemaphore;
EXTERN_FUNC PFN_vkDestroySemaphore ren_vkDestroySemaphore;

EXTERN_FUNC PFN_vkCreateSampler ren_vkCreateSampler;
EXTERN_FUNC PFN_vkDestroySampler ren_vkDestroySampler;

EXTERN_FUNC PFN_vkCreateDescriptorPool ren_vkCreateDescriptorPool;
EXTERN_FUNC PFN_vkDestroyDescriptorPool ren_vkDestroyDescriptorPool;
EXTERN_FUNC PFN_vkResetDescriptorPool ren_vkResetDescriptorPool;

EXTERN_FUNC PFN_vkAllocateDescriptorSets ren_vkAllocateDescriptorSets;
EXTERN_FUNC PFN_vkFreeDescriptorSets ren_vkFreeDescriptorSets;
EXTERN_FUNC PFN_vkUpdateDescriptorSets ren_vkUpdateDescriptorSets;

EXTERN_FUNC PFN_vkCreateQueryPool ren_vkCreateQueryPool;
EXTERN_FUNC PFN_vkDestroyQueryPool ren_vkDestroyQueryPool;
EXTERN_FUNC PFN_vkGetQueryPoolResults ren_vkGetQueryPoolResults;

EXTERN_FUNC PFN_vkCreateAccelerationStructureKHR ren_vkCreateAccelerationStructureKHR;
EXTERN_FUNC PFN_vkDestroyAccelerationStructureKHR ren_vkDestroyAccelerationStructureKHR;

EXTERN_FUNC PFN_vkCmdBeginRenderPass ren_vkCmdBeginRenderPass;
EXTERN_FUNC PFN_vkCmdBindPipeline ren_vkCmdBindPipeline;
EXTERN_FUNC PFN_vkCmdSetViewport ren_vkCmdSetViewport;
EXTERN_FUNC PFN_vkCmdSetScissor ren_vkCmdSetScissor;
EXTERN_FUNC PFN_vkCmdBindDescriptorSets ren_vkCmdBindDescriptorSets;
EXTERN_FUNC PFN_vkCmdBindVertexBuffers ren_vkCmdBindVertexBuffers;
EXTERN_FUNC PFN_vkCmdBindIndexBuffer ren_vkCmdBindIndexBuffer;
EXTERN_FUNC PFN_vkCmdDraw ren_vkCmdDraw;
EXTERN_FUNC PFN_vkCmdDrawIndexed ren_vkCmdDrawIndexed;
EXTERN_FUNC PFN_vkCmdEndRenderPass ren_vkCmdEndRenderPass;
EXTERN_FUNC PFN_vkCmdCopyBufferToImage ren_vkCmdCopyBufferToImage;
EXTERN_FUNC PFN_vkCmdCopyImageToBuffer ren_vkCmdCopyImageToBuffer;
EXTERN_FUNC PFN_vkCmdCopyBuffer ren_vkCmdCopyBuffer;
EXTERN_FUNC PFN_vkCmdFillBuffer ren_vkCmdFillBuffer;
EXTERN_FUNC PFN_vkCmdUpdateBuffer ren_vkCmdUpdateBuffer;
EXTERN_FUNC PFN_vkCmdPushConstants ren_vkCmdPushConstants;
EXTERN_FUNC PFN_vkCmdBlitImage ren_vkCmdBlitImage;
EXTERN_FUNC PFN_vkCmdClearColorImage ren_vkCmdClearColorImage;
EXTERN_FUNC PFN_vkCmdClearAttachments ren_vkCmdClearAttachments;
EXTERN_FUNC PFN_vkCmdCopyImage ren_vkCmdCopyImage;
EXTERN_FUNC PFN_vkCmdDispatch ren_vkCmdDispatch;
EXTERN_FUNC PFN_vkCmdDispatchIndirect ren_vkCmdDispatchIndirect;
EXTERN_FUNC PFN_vkCmdResetQueryPool ren_vkCmdResetQueryPool;
EXTERN_FUNC PFN_vkCmdWriteTimestamp ren_vkCmdWriteTimestamp;

EXTERN_FUNC PFN_vkCmdBeginRenderingKHR ren_vkCmdBeginRenderingKHR;
EXTERN_FUNC PFN_vkCmdEndRenderingKHR ren_vkCmdEndRenderingKHR;

EXTERN_FUNC PFN_vkCmdBuildAccelerationStructuresKHR ren_vkCmdBuildAccelerationStructuresKHR;
EXTERN_FUNC PFN_vkCmdWriteAccelerationStructuresPropertiesKHR ren_vkCmdWriteAccelerationStructuresPropertiesKHR;
EXTERN_FUNC PFN_vkCmdCopyAccelerationStructureKHR ren_vkCmdCopyAccelerationStructureKHR;
EXTERN_FUNC PFN_vkCmdTraceRaysKHR ren_vkCmdTraceRaysKHR;
EXTERN_FUNC PFN_vkCmdTraceRaysIndirectKHR ren_vkCmdTraceRaysIndirectKHR;

EXTERN_FUNC PFN_vkCmdBeginDebugUtilsLabelEXT ren_vkCmdBeginDebugUtilsLabelEXT;
EXTERN_FUNC PFN_vkCmdEndDebugUtilsLabelEXT ren_vkCmdEndDebugUtilsLabelEXT;
EXTERN_FUNC PFN_vkSetDebugUtilsObjectNameEXT ren_vkSetDebugUtilsObjectNameEXT;

EXTERN_FUNC PFN_vkCmdSetDepthBias ren_vkCmdSetDepthBias;

EXTERN_FUNC PFN_vkGetBufferMemoryRequirements ren_vkGetBufferMemoryRequirements;
EXTERN_FUNC PFN_vkGetAccelerationStructureBuildSizesKHR ren_vkGetAccelerationStructureBuildSizesKHR;
EXTERN_FUNC PFN_vkGetAccelerationStructureDeviceAddressKHR ren_vkGetAccelerationStructureDeviceAddressKHR;
EXTERN_FUNC PFN_vkGetRayTracingShaderGroupHandlesKHR ren_vkGetRayTracingShaderGroupHandlesKHR;

EXTERN_FUNC PFN_vkCreateRayTracingPipelinesKHR ren_vkCreateRayTracingPipelinesKHR;
}

#define VK_API_VERSION_MAJOR(version) (((uint32_t)(version) >> 22) & 0x7FU)
#define VK_API_VERSION_MINOR(version) (((uint32_t)(version) >> 12) & 0x3FFU)

namespace Ren {
class ILog;
bool LoadVulkan(ILog *log);
bool LoadVulkanExtensions(VkInstance instance, ILog *log);
} // namespace Ren
