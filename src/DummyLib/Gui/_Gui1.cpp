
#ifdef _MSC_VER
#pragma warning(disable : 4996)
#endif

#include "CaptionsUI.cpp"
#include "DebugInfoUI.cpp"
#include "DialogEditUI.cpp"
#include "DialogUI.cpp"
#include "PagedReader.cpp"
#include "SeqCanvasUI.cpp"
#include "SeqEditUI.cpp"
#include "TimelineUI.cpp"
#include "WordPuzzleUI.cpp"
